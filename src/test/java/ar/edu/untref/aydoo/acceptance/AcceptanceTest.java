package ar.edu.untref.aydoo.acceptance;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = false, tags={"~@wip"}, features = "src/test/features")
public class AcceptanceTest {
}
