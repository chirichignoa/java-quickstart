package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.Calculadora;
import org.junit.Test;
import org.junit.Assert;

public class CalculadoraTest {

    @Test
    public void sumarCaso1() {
        Calculadora calculadora = new Calculadora();
        int suma = calculadora.sumar(1,2);
        Assert.assertEquals(3, suma);
    }

    @Test
    public void sumarCaso2() {
        Calculadora calculadora = new Calculadora();
        int suma = calculadora.sumar(4,5);
        Assert.assertEquals(9, suma);
    }

}
